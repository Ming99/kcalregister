# KcalRegister

Keep track of your weight, water consumption & the ammount of kcal going in and out

## How does it work?

Starting up the app creates a folder, at root level, named "data". Inside will be created a file named {year}.json
The first file is named to the year it currently is. When a new date is selected from a year that had no file yet,
it will be created automatically.

### Default data

When a new file is created it contains 365 or 366 objects that look like:

{
    "date":"2024-01-01",
    "weight":0,
    "kcalIn":0,
    "kcalOut":0,
    "water":0
}

### Change data

By selecting the date you want to update, you simply fill in the form and press the 'Save'-button

You can also open the desired json-file and edit the objects from here.

## For developers

To test it in development us the start command in the package.json
"electron-forge start"

To make it an .exe use the package command in the package.json
"electron-forge package"