import React, { useEffect, useState } from "react";
import "../index.css";

export default function AddForm() {
    const [data, setData] = useState([]);

    const [formData, setFormData] = useState({});

    useEffect(() => {
        const dateNow = new Date();
        window.electron.ipcRenderer.send('read', dateNow.toISOString().split('-')[0]);
        window.electron.ipcRenderer.on('read-file-content', (response) => {
            if (response.success) {
                setData(response.content);
            } else {
                console.log(response.error);
            }
        });

        window.electron.ipcRenderer.send('read-date', dateNow.toISOString().split('T')[0]);
        window.electron.ipcRenderer.on('read-date-file-content', (response) => {
            if (response.success) {
                setFormData(response.content);
            } else {
                console.log(response.error);
                setFormData({
                    date: date,
                    weight: 0,
                    kcalIn: 0,
                    kcalOut: 0,
                    water: 0
                });
            }
        });

        return () => {
            window.electron.ipcRenderer.removeAllListeners('read-file-content');
            window.electron.ipcRenderer.removeAllListeners('read-date-file-content');
        }
    }, []);

    const handleChange = (e) => {
        const { name, value } = e.target;
        if (name === "date") {
            window.electron.ipcRenderer.send('read-date', value);
            window.electron.ipcRenderer.on('read-date-file-content', (response) => {
                if (response.success) {
                    setFormData(response.content);
                } else {
                    console.log(response.error);
                    setFormData({
                        date: date,
                        weight: 0,
                        kcalIn: 0,
                        kcalOut: 0,
                        water: 0
                    });
                }
            });
            return () => {
                window.electron.ipcRenderer.removeAllListeners('read-date-file-content');
            }
        }
        else {
            setFormData({
                ...formData,
                [name]: value
            });
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        // Create an object with the form data
        const dataObject = {
            date: formData.date,
            weight: parseFloat(formData.weight),
            kcalIn: parseFloat(formData.kcalIn),
            kcalOut: parseFloat(formData.kcalOut),
            water: parseFloat(formData.water)
        };

        window.electron.ipcRenderer.send('update', dataObject);
        window.electron.ipcRenderer.on('update-file-content', (response) => {
            if (response.success) {
                setFormData(response.content);
            } else {
                console.log(response.error);
            }
        });
        return () => {
            window.electron.ipcRenderer.removeAllListeners('update-file-content');
        }
    };

    return (
        <>
            {Object.keys(formData).length === 0 ? (
                <div>Information is loading.</div>
            ) : (
                <form onSubmit={handleSubmit}>
                    <div className="AddForm_container">
                        <div className="Form_input">
                            <label htmlFor="Date">Date:</label>
                            <input
                                type="date"
                                id="Date"
                                name="date"
                                value={formData.date.split('T')[0]}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="Form_input">
                            <label htmlFor="Weight">Weight:</label>
                            <input
                                type="number"
                                id="Weight"
                                name="weight"
                                value={formData.weight}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="Form_input">
                            <label htmlFor="Kcalin">Kcal in:</label>
                            <input
                                type="number"
                                id="Kcalin"
                                name="kcalIn"
                                value={formData.kcalIn}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="Form_input">
                            <label htmlFor="Kcalout">Kcal out:</label>
                            <input
                                type="number"
                                id="Kcalout"
                                name="kcalOut"
                                value={formData.kcalOut}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="Form_input">
                            <label htmlFor="Water">Water:</label>
                            <input
                                type="number"
                                id="Water"
                                name="water"
                                value={formData.water}
                                onChange={handleChange}
                            />
                        </div>
                    </div>
                    <button type="submit">Save</button>
                </form >
            )
            }
        </>
    );
}