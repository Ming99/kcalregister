import { app, BrowserWindow, ipcMain } from 'electron';
const fs = require('fs');
const path = require('path');

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  app.quit();
}

const createWindow = () => {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1000,
    height: 600,
    webPreferences: {
      preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY,
    },
    autoHideMenuBar: true,
  });

  // and load the index.html of the app.
  mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);

  // Open the DevTools.
  mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow();

  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.


/*
    * Requires:
    * year: 2024 (int)
    *
    * Description:
    * Generate for the given year for all days a object containing the date, weight, kcalIn, kcalOut and water
    *
    * return:
    * array[{
            date: "2024-01-01",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }, {
            date: "2024-01-02",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }, {
            date: "2024-01-03",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }]
*/
const generateYearData = (year) => {
  const yearData = [];
  const startDate = new Date(`${year}-01-01T00:00:00.000Z`);
  const endDate = new Date(`${year}-12-31T00:00:00.000Z`);

  for (let date = new Date(startDate); date <= endDate; date.setDate(date.getDate() + 1)) {
    const formattedDate = date.toISOString().split('T')[0];
    yearData.push({
      date: formattedDate,
      weight: 0,
      kcalIn: 0,
      kcalOut: 0,
      water: 0
    });
  }

  return yearData;
}

/*
    * Requires:
    * filePath: '{__dirname}/data/{year}.json'
    * content: array[{
            date: "2024-01-01",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }, {
            date: "2024-01-02",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }, {
            date: "2024-01-03",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }]
    *
    * Description:
    * Check if file exists or creates the path and file
    * Content will be generated according to what year is requested
    *
    * Return:
    *
*/
const writeFile = (filePath, content) => {
  // Write JSON data to file
  fs.writeFileSync(filePath, JSON.stringify(content), (err) => {
    if (err) throw err;
  });
}

/*
    * Requires:
    * year: 2024 (int)
    *
    * Description:
    * Check if file exists or creates the path and file
    * Content will be generated according to what year is requested
    *
    * Return:
    * filePath (string)
*/
const ensureFileExists = (year) => {
  const folderPath = path.join('./', 'data');
  const filePath = path.join(folderPath, `${year}.json`);
  const content = generateYearData(year);

  // Ensure the folder exists
  if (!fs.existsSync(folderPath)) {
    fs.mkdirSync(folderPath);
    writeFile(filePath, content);
  }

  return filePath;
}

/*
    * Requires:
    * year: 2024 (int)
    *
    * Description:
    * Try to find and read the file in ./data that has the name `${year}.json`
    *
    * return:
    * array[{
            date: "2024-01-01",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }, {
            date: "2024-01-02",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }, {
            date: "2024-01-03",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }]
*/
const read = (year) => {
  const filePath = ensureFileExists(year);
  const fileContent = fs.readFileSync(filePath, 'utf8');
  return JSON.parse(fileContent);
}

/*
    * Requires:
    * year: 2024 (int)
    *
    * Description:
    * Try to find and read the file in ./data that has the name `${year}.json`
    *
    * return:
    * array[{
            date: "2024-01-01",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }, {
            date: "2024-01-02",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }, {
            date: "2024-01-03",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }]
*/
ipcMain.on('read', (event, year) => {
  try {
    const data = read(year);
    event.reply('read-file-content', { success: true, content: data });
  } catch (err) {
    const errorString = err.message + " ---- " + filePath;
    event.reply('read-file-content', { success: false, error: errorString });
  }
});

/*
  * Requires:
  * date: "2024-01-01"
  * dataSet: [{
            date: "2024-01-01",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }, {
            date: "2024-01-02",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }, {
            date: "2024-01-03",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }]
  * 
  * Description:
  * From the given array the object with the same date will be returned if found
  * Else a empty object will be returned
  * 
  * Return:
  * found => {
            date: "2024-01-03",
            weight: 0,
            kcalIn: 0,
            kcalOut: 0,
            water: 0
        }
  * Not found => {}
*/
const findObject = (date, dataset) => {
  let result = {};
  dataset.forEach(set => {
    // set.date needs to be substringed to get only the date
    if (set.date === date)
      result = set;
  });
  return result;
}

/*
    * Requires:
    * date: "2024-01-01"
    *
    * Description:
    * Find specific date
    *
    * return:
    * {
        date: "2024-01-01",
        weight: 0,
        kcalIn: 0,
        kcalOut: 0,
        water: 0
    * }
*/
ipcMain.on('read-date', (event, date) => {
  try {
    const year = date.split('-')[0];
    // Get the content of the requested json file
    const content = read(year);
    const requestedObject = findObject(date, content);

    event.reply('read-date-file-content', { success: true, content: requestedObject });
  } catch (err) {
    event.reply('read-date-file-content', { success: false, error: err });
  }
});

const rewriteContent = (data, content) => {
  let index = -1;
  // Find index
  for (let i = 0; i < content.length; i++)
    if (content[i].date === data.date)
      index = i;
  // Update content
  if (index >= 0)
    content[index] = data;

  return content;
}

/*
    * Requires:
    * data: {
        date: "2024-01-01",
        weight: 0,
        kcalIn: 0,
        kcalOut: 0,
        water: 0
    * }
    *
    * Description:
    * Updates the right file based on the date that is given in data.date
    * data.date contains the year, so this substring will be used for the `./data/${year}.json`
    *
    * Return:
    *
*/
ipcMain.on('update', (event, data) => {
  try {
    // Get content of right file
    const date = data.date;
    const year = date.split('-')[0];
    const folderPath = path.join('.', 'data');
    const filePath = path.join(folderPath, `${year}.json`);
    let content = read(year);

    // Update correct object in the content
    const updatedContent = rewriteContent(data, content);

    // Overwrite the json file
    writeFile(filePath, updatedContent);

    // Get updated content of the right file
    // Even though this should be the same as updatedContent
    const newContent = read(year);
    const newRequestedObject = findObject(date, newContent);

    event.reply('update-file-content', { success: true, content: newRequestedObject });
  } catch (err) {
    const errorString = err + " ---- " + filePath;
    event.reply('update-file-content', { success: false, error: errorString });
  }
});