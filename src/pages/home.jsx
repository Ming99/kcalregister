import React from "react";
import AddForm from "../components/AddForm.jsx";
import Diagram from "../components/Diagram.jsx";
import Overview from "./Overview.jsx";

export default function Home() {
    return (
        <>
            <Diagram />
            <AddForm />
            <Overview />
        </>
    );
}